<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Issue;

class Book extends Model
{
    protected $fillable = [
        'book_name', 
        'isbn_number',
        'auther_id',
        'book_rent',
        'book_quantity'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
public function user()
{
  //$this->belongsTo(User::class,'id', 'auther_id');
  $this->hasOne('App\Book');
}

public static function getBookAvailability() {
      
        $bookList = DB::table('books')
                    ->select('id',
                            'book_name',
                            'isbn_number',
                            'book_author',
                            'book_quantity'
                        )->orderBy('id', 'asc')->get();
        
             $bookList =  $bookList ->toArray();
             $data = [];
             
            foreach ($bookList as $i => $book) {
                $data[$i]['id'] = $book->id;
                $data[$i]['book_name'] = $book->book_name;
                $data[$i]['isbn_number'] = $book->isbn_number;
                $data[$i]['book_author'] = $book->book_author;
                $data[$i]['total_book'] = $book->book_quantity;
                $issuedBook = array(
                    'book_id' => $book->id,
                    'retrun_status' => 0
                );
                
                $returnBook = array(
                    'book_id' => $book->id,
                    'retrun_status' => 1
                );
               
                $data[$i]['issue_book'] = Issue::getIssueBookCounter($book->id);
                $data[$i]['return_book'] = Issue::getReturnedBookCounter($book->id);
                $data[$i]['avaliable_book'] = $book->book_quantity - $data[$i]['issue_book'];
                
        }
        
        return $data;
}

}
