<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssuedBookDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issued_book_details', function (Blueprint $table) {
            $table->id();
            $table->integer('book_id');
            $table->integer('user_id')->nullable();
            $table->timestamp('issues_date')->nullable();
            $table->timestamp('return_date')->nullable();
            $table->integer('amount_charged')->nullable();
            $table->tinyInteger('retrun_status')->comment('1 for returned book, 0 for hold book');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_book_details');
    }
}
