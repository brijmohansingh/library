<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'PassportAuthController@register');
Route::post('login', 'PassportAuthController@login');
 
Route::middleware('auth:api')->group(function () {
    Route::resource('posts', 'PostController');
    Route::resource('books', 'BookController');
    Route::post('isssue-book', 'IssueBooks');
    Route::post('return-book', 'ReturnBooks');
    Route::get('total-rent/{issueId}', 'TotalRent');
    Route::post('book-summary', 'Search');
    
 
});