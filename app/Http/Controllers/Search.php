<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Issue;
use DB;

class Search extends Controller {

    public function __invoke(Request $request) {

        if (empty($request->from)) {
            return response()->json([
                        'success' => false,
                        'message' => 'Bad Requrst'
                            ], 500);
        }
                
        $fromDate = (new Carbon($request->from))->format('Y-m-d');

        if (isset($request->to)) {
            $toDate = (new Carbon($request->to))->format('Y-m-d');
            $toDate = $toDate . ' 23:59:59';
        } else {
            $toDate = $fromDate . ' 23:59:59';
        }

        $fromDate = $fromDate . ' 00:00:00';
        
            
            
        return response()->json([
                   'success' => true,
                   'data' => Issue::assignedBooks($fromDate, $toDate)
       ]);
            
            
              
              
           

    }
}
