<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Issue;
use App\Rules\AllowBook;

class IssueBooks extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {


        $validator = Validator::make($request->all(), [
                    'book_id' => ['required', 'numeric', new AllowBook],
                    'user_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->messages()
                            ], 500);
        }


        $issue = new Issue();
        $issue->book_id = $request->book_id;
        $issue->user_id = $request->user_id;
        $issue->amount_charged = null;
        $issue->retrun_status = 0;
        $issue->return_date = Carbon::now()->toDateTimeString();

        if (auth()->user()->issues()->save($issue)) {
            return response()->json([
                        'success' => true,
                        'data' => $issue->toArray()
            ]);
        } else {
            return response()->json([
                        'success' => false,
                        'message' => 'Book has not been assigned to user'
                            ], 500);
        }
    }

}
