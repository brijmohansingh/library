<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Issue;

class TotalRent extends Controller {

    public function __invoke($issueId) {

        if (empty($issueId)) {
            return response()->json([
                        'success' => false,
                        'message' => 'Bad Requrst'
                            ], 500);
        }

        $issueBook = Issue::find($issueId);
        $issuesDate = new Carbon($issueBook->issues_date);
        $days = ceil($issuesDate->diffInHours(Carbon::now()) / 24);
        $issueBook->amount_charged = 2 * $days;
        $paid = false;

        if ($issueBook->return_date != null && $issueBook->retrun_status == 1) {
            $paid = true;
        }

        if ($days) {
            return response()->json([
                        'success' => true,
                        'total_rent' => $issueBook->amount_charged,
                        'paid' => $paid,
                        'data' => $issueBook->toArray()
            ]);
        } else {
            return response()->json([
                        'success' => false,
                        'message' => 'Invalid Book ID entered or book already returned'
                            ], 500);
        }
    }

}
