<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;
use Exception;
use App\Issue;
use App\Rules\AllowBook;

class ReturnBooks extends Controller {

    public function __construct() {

        if (auth()->check()) {
            return response()->json([
                        'success' => false,
                        'message' => 'Bad Requrst'
                            ], 401);
        }
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {
        
        try {
            $issueId = $request->issue_id;
            $issue = auth()->user()->issues()->find($issueId);

            if (!$issue) {
                throw new Exception('Invalid Book ID entered or book already returned');
            }

            $conditions = array(
                'id' => $issueId,
                'retrun_status' => 0
            );



            if (Issue::getIssueBookById($issueId) === false) {
                throw new Exception('Invalid Book ID entered or book already returned');
            } else {
                $issue = Issue::where($conditions)
                        ->firstOrFail();
               
                DB::transaction(function() use($issueId) {
                    $issueBook = Issue::find($issueId);
                    $issueBook->return_date = Carbon::now()->toDateTimeString();
                    $issuesDate = new Carbon($issueBook->issues_date);
                    $days = ceil($issuesDate->diffInHours(Carbon::now()) / 24);
                    $issueBook->amount_charged = 2 * $days;
                    $issueBook->retrun_status = 1;
                    
 
                    if (auth()->user()->issues()->save($issueBook)){
                        return response()->json([
                                    'success' => true,
                                    'data' => $issueBook->toArray()
                        ]);
                    } else {
                         return response()->json([
                                    'success' => false,
                                    'message' => 'Book not returned'
                                        ], 500);
                        
                    }
                        
                    
                    
                });
            }
        } catch (Exception $e) {

            return response()->json([
                        'success' => false, 
                        'message' => $e->getMessage()
                            ], 404);
        }
    }

}
