<?php

namespace App;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Post;
use App\Book;
use App\Issue;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    
    public function books()
    {
        return $this->hasMany(Book::class);
    }
    
    public function issues()
    {
        return $this->hasMany(Issue::class);
    }
    
//    public function isLogged()
//    {
//        $user = (auth()->user())->id;
//        
//        if ($user->id) {
//           return true; 
//        }
//        
//        return false;
//
//print();
//    }
    
    
}
