<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Issue;


class AllowBook implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        
        $bookAllowCounter = Issue::getBookCounterOfUser($value);
        if ($bookAllowCounter > 4) {
            return false;
        }
        
         return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'More than 5 book will not issue per user';
    }
}
