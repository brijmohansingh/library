<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller {

    public function __construct() {

        if (auth()->check()) {
            return response()->json([
                        'success' => false,
                        'message' => 'Bad Requrst'
                            ], 401);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return response()->json([
                    'success' => true,
                    'data' => Book::getBookAvailability()
        ]);
    }

    public function show($id) {
        $book = auth()->user()->books()->find($id);

        if (!$book) {
            return response()->json([
                        'success' => false,
                        'message' => 'Post not found '
                            ], 400);
        }

        return response()->json([
                    'success' => true,
                    'data' => $book->toArray()
                        ], 400);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'book_name' => 'required',
                    'book_quantity' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->messages()
                            ], 500);
        }

        $book = new Book();
        $book->book_name = $request->book_name;
        $book->isbn_number = $request->isbn_number;
        $book->book_author = $request->book_author;
        $book->user_id = $request->user_id;
        $book->book_rent = 2;
        $book->book_quantity = $request->book_quantity;

        if (auth()->user()->books()->save($book))
            return response()->json([
                        'success' => true,
                        'data' => $book->toArray()
            ]);
        else
            return response()->json([
                        'success' => false,
                        'message' => 'Book not added'
                            ], 500);
    }

    public function update(Request $request, $id) {

        $book = auth()->user()->books()->findOrFail($id);

        if (!$book) {
            return response()->json([
                        'success' => false,
                        'message' => 'Post not found'
                            ], 400);
        }

        $updated = $book->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                        'success' => true,
                        'data' => $book->toArray()
            ]);
        } else {
            return response()->json([
                        'success' => false,
                        'message' => 'Post can not be updated'
                            ], 500);
        }
    }

    public function destroy($id) {
        $book = auth()->user()->books()->find($id);

        if (!$book) {
            return response()->json([
                        'success' => false,
                        'message' => 'Product not found'
                            ], 400);
        }

        if ($book->delete()) {
            return response()->json([
                        'success' => true
            ]);
        } else {
            return response()->json([
                        'success' => false,
                        'message' => 'Post can not be deleted'
                            ], 500);
        }
    }

}
