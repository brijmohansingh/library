<?php

namespace App;

use DB;
use App\Book;
use Illuminate\Database\Eloquent\Model;

class Issue extends Model {

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'issued_book_details';

    public static function getBooks() {
        
    }

    public function user() {
        // $this->belongsTo(User::class);
        $this->hasOne('App\User');
    }

    public static function getIssueBookCounter($bookId = null) {
        $condition = array(
            'book_id' => $bookId,
            'retrun_status' => 0
        );
        return DB::table('issued_book_details')->select()
                        ->where($condition)
                        ->count();
    }

    public static function getBookCounterOfUser($bookId = null) {
        $condition = array(
            'book_id' => $bookId,
            'user_id' => auth()->id(),
            'retrun_status' => 0
        );
        return DB::table('issued_book_details')->select()
                        ->where($condition)
                        ->count();
    }

    public static function getReturnedBookCounter($bookId = null) {
        $condition = array(
            'book_id' => $bookId,
            'retrun_status' => 1
        );
        
        return DB::table('issued_book_details')->select()
                        ->where($condition)
                        ->count();
    }
    
    public static function getIssueBookById($issueId = null) {
        
        $condition = array(
            'id' => $issueId,
            'retrun_status' => 0
        );
        $count = DB::table('issued_book_details')->select()
                ->where($condition)
                ->count();
        
        if ($count) {
            return true;
        } else {
            return false;
        }
    }
    
    
    public static function assignedBooks($fromDate , $toDate) {

         $conditions = [
                'retrun_status' => 0,
                'fromDate' => $fromDate,
                 'toDate' => $toDate
            ];
        
        
            $sqlQuery = "select 
                         `books`.`id` as id,
                         `books`.`book_name` as name,
                         count(`books`.`id`) as  quantity,
                         `users`.`name` as personName
                    from `books` 
                     inner join `issued_book_details` ON `issued_book_details`.`book_id` = `books`.`id`
                     inner join `users` ON `users`.`id` = `issued_book_details`.`user_id` 
                   Where 
                        issued_book_details.retrun_status = :retrun_status
                        AND issued_book_details.issues_date >= :fromDate
                        AND issued_book_details.issues_date <= :toDate
                        GROUP BY `users`.`id`, `books`.`id`,`books`.`book_name`
                        ORDER BY issued_book_details.id ASC";
            
            $data = [];
            $data['books_issued'] = DB::select(DB::raw($sqlQuery), $conditions);
            
            
             $conditions = [
                'retrun_status' => 1,
                'fromDate' => $fromDate,
                 'toDate' => $toDate
            ];
             
             
           $sql = "select 
                         `books`.`id` as id,
                         `books`.`book_name` as name,
                         count(`books`.`id`) as  quantity,
                         issued_book_details.amount_charged as amountCharged,
                         SUM(issued_book_details.amount_charged) as totalAmountChargedOnDay,
                         `users`.`name` as personName
                    from `books` 
                     inner join `issued_book_details` ON `issued_book_details`.`book_id` = `books`.`id`
                     inner join `users` ON `users`.`id` = `issued_book_details`.`user_id` 
                   Where 
                        issued_book_details.retrun_status = :retrun_status
                        AND issued_book_details.issues_date >= :fromDate
                        AND issued_book_details.issues_date <= :toDate
                        GROUP BY `users`.`id`, `books`.`id`,`books`.`book_name` , date(`issued_book_details`.`return_date`)
                        ORDER BY issued_book_details.id ASC";
          
          
          
          
            $data['books_returned'] = DB::select(DB::raw($sql), $conditions);
            return $data;
    }

}
